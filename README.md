For now, just a folder of markdown files :)

### Documentation style

- <https://documentation.divio.com/>
- <https://diataxis.fr/>

### Choice of doc engine

Roughly sorted in terms of ease of use and what could make sense:

- MkDocs - SSG. Markdown and a YAML config. 18k. also has mkdocstrings. see also [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/) framework for "good client-side search, page tags, or easy support for a lot of markdown extensions (e.g. code syntax highlighting, admonitions/callouts)." Example: [pydantic docs](https://docs.pydantic.dev/latest/)
- Sphinx (6k GitHub stars). ReST. Useful for Python because of things like [autodoc](https://www.sphinx-doc.org/en/master/tutorial/automatic-doc-generation.html). Used for Linux kernel, Apple's Swift, LLVM, etc. MyST started as an extension for Sphinx (so one can use a markdown flavor with Sphinx using that extension).
- [mdBook](https://rust-lang.github.io/mdBook/). CommonMark with extensions. Popular in Rust ecosystem. Like gitbook (which went proprietary). Minimalist. 16k.
- [Jupyter-Book builds on Sphinx](https://jupyterbook.org/en/stable/explain/sphinx.html). Sphinx uses ReST, JB uses MyST Markdown. 3.6k stars. [Example docs](https://arcdocs.leeds.ac.uk/welcome.html)
- [Quarto](https://quarto.org/) : Jupyter and Pandoc MD.  3k
- Docsify 26k markdown SSG
- [Docsy](https://github.com/google/docsy) : a hugo docs template. 2.6k
- [Starlight](https://github.com/withastro/starlight), a framework on the Astro SSG
- [Docusaurus](https://gitlab.com/antora/antora) closely tied to npm, heavyweight. 50k.
- Bookdown - tied to RMarkdown
- [Antora](https://gitlab.com/antora/antora) asciidoctor. transpiles to docbook XML
- Docbook is very unpopular because of the clunky XML format

[Or other static site generators](https://jamstack.org/generators/)... (Nextra, Gatsby, Slate, and many, many, more)

Or wiki software, like [BookStack](https://github.com/BookStackApp/BookStack) (geared for documentation, touts its simplicity).
