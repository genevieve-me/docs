## Open Society Foundation (OSF)

Todo

# Updating client archives

## Friedrich Ebert Stiftung (FES)

The archive is publicly available at <https://futureofwork.fes.de/digital-archive>. It is (by convention, manually) mirrored at [the "FES archive" Google Sheet"](https://docs.google.com/spreadsheets/d/1Qhk9X8SXa5y3ZMiSvXsGfnIfhG0F9JZdxFREaLp-ZKA/edit#gid=1595357051) so that manually curated content can easily be added by any user.

When there is new content to upload, it is sent to the archive via POST to an API endpoint. Note that the whole csv must be uploaded to the API: if you send a file with new content, it will *_not_* update the archive, it will **delete** the old content!

The updated archive should be POSTed to the `https://search.the-syllabus.com/api/v1/fes/index` API endpoint.
The endpoint requires a bearer authentication header (the token is static and shared).
It takes a multipart form with a `file`, `user`, and `password`. The file is of type `text/csv` and the credentials
are the same as for [the website admin panel](https://admin.the-syllabus.com/login).

If you prefer to use an app like postman, we have two .json configs for it: the postman collection and the production postman environment. Download the postman client and import both of them.
You will see "PROD" under Environments and "Search" under Collections. (left sidebar).

We want to use the "index" API endpoint (under Collections/Search). In the top right, select the "PROD" environment.

POST to `{{searchUrl}}/api/v1/fes/index` with Body type `form-data`. It takes three parameters: `file` (a `.csv` file); and `user`/`password` (the current Syllabus admin panel email and password).

## Center for Digital Rights (CDR)

The archive is publicly available at <https://search.digitalrightsarchive.net/>.

You need to update [this sheet](https://docs.google.com/spreadsheets/d/1wXOUvZ-wfgcRMlwNJsg6CAKtRALn_Gkdik5tSo0TU4E/edit#gid=153406143) with the new selections shared in the relevant channel, download them as cdr.csv, and then [upload here](https://github.com/The-Syllabus/covidsearch/tree/master/src/main/resources)

There's [an Amazon pipeline](https://eu-west-1.console.aws.amazon.com/codesuite/codepipeline/pipelines/covid19-search/view?region=eu-west-1) where you can see the staus of the upload (and the specific errors if those come up).

This pipeline shows the status of the update. It only triggers on commits to the github repo.

**TODO:** weirdly enough it failed on the FES csv. But this repo has nothing to do with the FES archive - that is purely updated via the API endpoint. So why is that being read (and why is it even there?)

I'm going to try just deleting the FES csv from the repo and seeing if that fixes it.

## Open Society Foundation (OSF)

**TODO:** does this (or other DBs) even exist?
