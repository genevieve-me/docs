We use commands in Django as a main method of launching tools. All the possible commands have the following .py script path pattern: tsbackend/<app_name>/management/commands/<command_name>.py.All the commands are usually launched from a command line and virtual environment with the pattern: `python manage.py <command_name> --\<param\> \<value\>`

An example of a typical command for scraping queries: python manage.py process_queries --setup DA --restart. Final results of scraping queries are stored in the source table in the database. There also are other auxiliary tables, that connect query_setup table with source table. This intermediate table stores information about what query produced a source, when it was scraped, etc. For a deeper understanding, please, visit our GitLab Wiki page, see source code of tsbackend/cse/management/commands/create_cse_output.py and check SQL commands in our Wiki.

See also [the docs on Google Custom Search queries](docs/google-cse.md).

### API keys

**TODO:** is this up to date?

A very important feature of CSE and YT scraping is that they need Google API keys to produce results. We have a special app - Keys. This is a Django application, whose purpose is to organize and manage all the Google API keys efficiently. It has 3 main tables in a database:
google_account - it contains data about Google accounts that we have (login, encrypted password, backup email)
key - it contains all the Google API keys we have without linking to certain services yet, but linked to the respective Google accounts.
X_key - several tables that contain API keys for certain services each. Keys usages per day are stored in such tables. For now, we have cse_key and youtube_key tables.
All the API key usages are reset to zero by Google when it's 00:00 PTC, we do it too in the database with the help of a cron-job. Different API keys have their own limits. It depends on the API we use. CSE API keys have a limit of 100 requests (“units”) per day. YouTube API is a bit more complex as we use different functions of this API that cost different amounts of “units”.
Scrapers and key classes are developed in the way that they use API keys efficiently and no conflict can appear. We have many different API keys and sometimes it happens that accounts, that are related to the keys we have, get blocked. We can also reach the limit of our key capacity and it turned out to be impossible to scrape anything.


# Academic Article Scraper

Introduction
-----------------------

The Academic Article Scraper is a tool for retrieving metadata about articles whose URL is known. It uses **Crossref API**, **Unpaywall API**, **publisher websites** and **Google Scholar** to get that data.

Inputs
--------------------------

The tool reads input files fed using the `--input_file` argument or the contents of the [English Article reviewer sheet](https://docs.google.com/spreadsheets/d/1jY6KBdvLYagILmszuFFNVHpXi-NScbCakayxmJNXIUg/edit#gid=0).

The input is expected to conform to The Syllabus' UCSV format.

Commands
----------------------

### 1. Scrape Academic Articles
> Command: `python manage.py scrape_academic_articles` (`--input_file`, `--output_file`, `--download_from_gsheets`, `--upload_to_gsheets`, `--url`, `--filter_item_type`, `--continue_backup`, `--preserve_blanks`,`--retry_abstracts`, `--use_proxies`, `--include_cr_fields`)

This command constitutes the main article scraper flow. It iterates over the rows provided from the input source and attempts to retrieve metadata for that article. It does this first by attempting to isolate a DOI in the URL or from the contents of the resource itself. This is then fed to CrossRef, Unpaywall and Google Scholar. Where a DOI/API data is missing further retrieval is attempted from the resource. 

## Arguments
* `--input_file`/`--output_file` - these take input and output file paths
* `--download_from_gsheets`/`--upload_from_gsheets` - these toggle whether to use Google Sheets input/output sources
* `--continue_backup` - toggles whether to continue scraping from the most recently outputted backup file
* `--url` - this takes an individual URL. It is useful for debugging purposes
* `--filter_item_type` - toggles whether only to process A1/A2 labelled items
* `--preserve_blanks` - toggles whether to preserve records which are missing from CrossRef
* `--retry_abstracts` - toggles whether to process rows which are missing abstracts or only those which are missing titles
* `--use_proxies` - toggles whether to use proxy servers for improved coverage
* `--include_cr_fields` - toggles whether to include the non-UCSV issn-print/issn-online/CrossRef subject fields

### 2. Scrape Article References
> Command: `python manage.py scrape_article_references` - **REPLICATES MAIN COMMAND ARGUMENTS** 

This command extracts references from article URLs in an input file and then applies the article scraping command to those references. As such, the command logic replicates the main article scraping command precisely.

### 3. Scrape Missing Abstracts
> Command: `python manage.py scrape_missing_abstracts` (`--input_file`, `--output_file`)

**PENDING**

This command does more intensive scraping for those articles from specified publishers which are missing abstracts.
