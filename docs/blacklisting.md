There are two types.

We run our results through a google sheet which has formulas to match certain strings (a "blacklist table").
If these formulas return a match, we omit that row.

There is also programmatic blacklisting on the level of the scraper. **TODO**: how does that work?
