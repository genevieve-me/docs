# Data sources

#### Academic Articles

- Google scholar alerts. These are a manual flow which currently just get pasted into Slack by Oliver every week
- Google Custom Search Engine (aka Programmable Search Engine) results on known-good domains. Certain query sets are dedicated to academic articles
- CrossRef : An article scraper repo (**TODO:** can't find link) that sends queries to it
- OpenEdition. We just launched a dedicated scraper, not in the flow yet
- SerpAPI. It's very expensive, so we only use it for foreign languages, called GScholar API in the backend (that's a misleading name, it's for [SerpAPI's Google Scholar API](https://serpapi.com/google-scholar-api))

Potentially take a look at [OpenAlex](https://help.openalex.org/about-us)

#### Documents

- CSE queries
- SSRN.com repository of working papers. Query with a dedicated scraper
- Funnelweb crawling/spidering (eventually)

#### Journalism

- CSE queries (eventually rework CSE queries, introduce postprocessing, and reduce reliance for...)
- Funnelweb crawling/spidering (eventually)

#### Books

- Amazon book scraping via the backend. Uses the same query setups system as CSE scraping, but other query sets dedicated 
- Google Scholar Alerts and/or SerpAPI also sometimes returns books with DOIs that are on Google Scholar.
- Local scrapers for a variety of local booksellers, mostly for foreign languages. **TODO**: add repository links
- <https://www.librest.com/> - French
- <https://www.todostuslibros.com/> - Spanish
- <https://www.dnb.de/EN/Home/home_node.html> - German
- <https://www.libreriauniversitaria.it/> - Italian

If funnelweb can work on book publisher websites, we want to retire that complicated castle of scrapers. Amazon results are especially bad.
Ideally, we scraper all the **publishers** directly, not any resellers. Maintaining filters on websites is a nightmare.

#### Podcasts

- Custom RSS [handler in the backend](https://gitlab.com/the-syllabus-scripts/backend/-/tree/master/tsbackend/podcasts)
- Inoreader (will be replaced by custom tool)
- Soundcloud: has a lot of spam
- ListenNotes [scraper](https://gitlab.com/the-syllabus-scripts/podcast-scraper). ListenNotes API is expensive

#### Videos

- Youtube
- Vimeo

## Google tools

#### Google scholar alerts

-   A hack, google cracks down on it and sometimes closes inactive
    > accounts (that we were still relying on)

-   We have hundreds of such google accounts, all subscribed to maximum number of alerts, each alert on maximum number of queries, each query on max number of characters

-   When they receive alert emails, we have auto-forwarding rules to some other accounts (with certain filters)

-   Those inboxes are being scraped [(repository link)](https://gitlab.com/the-syllabus-scripts/gsa-email-downloader), we then have [a tool to parse them](https://gitlab.com/the-syllabus-scripts/alert-parser). Oliver is manually running this and posts the results on Slack every tuesday

#### Google custom search engines (CSE)

-   Until we start using funnelweb, 100% of our journalism content comes
    > from CSEs

-   Example of query structure: labour technology \| infrastructure \|
    > automated

-   \| is an OR, but whitespace is an AND, so a query like labour
    > technology \| infrastructure \| automated becomes labour AND
    > (technology OR infrastructure OR automated)

-   Many of our queries combine a labor-related term with a tech-related
    > term. Still a bit more generic than is ideal, but many of our
    > topics are niche enough and our CSEs are limited enough that we
    > can afford to run such queries without ending up with huge
    > outputs.

-   The reason why we use a lot of \| , instead of doing the equivalent
    > of the above which would be 3 separate queries, is that we save on
    > API calls and scraping length

#### Funnelweb

See [funnelweb docs](funnelweb.md).
