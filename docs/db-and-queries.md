# Database

The database lives on AWS and can be explored with DBeaver (or a similar
DB tool of one's choice). Funnelweb connects to it: this connection is
configured using the postgres settings in the config.yml. Funnelweb
tables are called public.funnelweb (stores old URLS) and
public.funnelweb_run (which stores metadata about the runs). If you
somehow do some kind of 'bad run', you can use the run's id to delete
all the URLs that it generated...

For each run, the date/time is stored, some sort of UUID is generated,
and then the JSON config is put into funnelweb_run and all the bad URLs
are put into funnelweb. The date isn't reliable but also not really
needed -- we just need to know it existed at the time funnelweb was run.
We don't always end up with a timestamp for every url, that's ok. The id
is just first 8 chars from a uuid4, used as foreign key to link urls to
the run that generated them, so you can find the config used to generate
a url if need be. When we do start running in normal mode, it does all
this, but also sends the content urls to openai to extract metadata, so
we can get it into the review sheet for reviewers, and also apply
scoring/classification measures eventually

Regarding use of funnelweb, it's handy to know that config values not
set in a specific domain fallback to the general config. So in general
you can set disabled: true, then if you're just testing one domain, for
that domain only set disabled: false and all other domains will be
ignored. You can also split into numerous config files it if helps you.
Any way you want to organise for development is fine, but eventually in
prod we should have one giant YAML file with all domains properly
configured.

### Important tables

-   Cse_id ( we seem to use id 1 for everything)

-   Funnelweb, funnelweb_content, funnelweb_run

**TODO:** describe the shape of funnelweb tables

## Query Setups

Queries are sent to search engines (Google, Amazon, etc.) and produce
sources: we store this connection between query and source in the
database. Scraping queries is one of the most significant methods of
finding new content.

Each query set-up is associated with one of our 60 modules. There is also a "testing" module, for work-in-progress query-setups that we don't want to pollute our per-module data with.

**Todo: some of this is outdated, from the 2021 doc**

Query Setup : a list with queries. Usually, a setup contains queries
related to one language or one general topic. Scrapers usually work with
query setups, but not with separate queries. You can't scrape a query,
but can scrape a setup of queries.

Query Set : a bunch of actual queries inside a query setup. It's
possible to add and delete queries from a query setup. To preserve a
connection between sources and query setups, we cannot delete a query,
but we can disable it. The idea is that a query set is a slice of active
queries at some period of time. One query setup may have dozens of query
sets, but only the newest one will work, because all the other sets are
outdated. Once you update a query setup with default tools, a new query
set with actual queries will be created. So actually when you want to
scrape a query setup, you scrape its last query set's queries.


In the same vein, individual queries can optionally have an associated language, and boolean tags `is_academic` and `is_niche`. The use of these tags, including the "unit," makes reviewing easier for curators.

Subpage - content hosted on a domain, e.g.,
bbc.com/politics/western-europe/ger. Default domain's subpage in the
database has URL NULL, which means that this is a default domain. A
subpage may have several tags. Each subpage may have its own item_type
(A-3, J-5, D-2, etc.). There are the following spreadsheets with already
classified domains/subpages:

• Articles - article_type_legend_and_lists

• Books - book_type_legend_and_lists

• Document - document_type_legend_and_lists

• Journalism - journalism_type_legend_and_lists

• Podcast - podcast_type_legend_and_lists

• Video - video_type_legend_and_lists

A source can also have some or all of the following associated with it. Of course, some might be unknown (e.g. Language), left blank (Unit), or non-existent (Query, for hand-curated content).

- Content type - every source has this field.

- Item type - it's defined by a domain where a source is published.

- Query - a source is found while queries search execution. Not all the
sources have queries, because sources can be found using other methods
(checking our YT account's subscriptions, etc.).

- Unit - an optional text token, generally a topic or client, associated with individual queries. They can be any string, like "climate" or "X Foundation." Units are stored in the `unit` table in our database, meaning that they can exist without being related to other entities in the system.

- Language - a language of a source's content.

And some deprecated attributes:

- Tag - a specific topic that relates to a domain/subpage. **Not in use anymore!**

Some sources also have content-type specific fields and that's why such
additional data is stored in additional tables. For example, YouTube
videos are connected with YouTube channels and it doesn't make sense to
create a YT channel field to all the sources, that's why video sources
from YT also have several additional tables that serve them.

## Blacklists

The "blacklist" actually refers to both blacklists and whitelists. For
example, we have blacklists for title words (gaming), URL parts
(news/gossips/) and domains (pcgaming.com).

In the database, we have SQL records, with the following columns:

-   Target field: every blacklist element should be applied to a certain
    > target field, e.g. a YouTube channel title or a source url. Each
    > target field is associated with an integer, and the mappings are
    > in the blacklist_target_field table.

-   list_status either B or W (for blacklist or whitelist)

-   case_sensitive true or false (if so, items will be converted to
    > lowercase before insertion in the database)

-   spreadsheet_uri and sheet_uri, google sheets containing blacklist
    > items

The actual blacklists live in those corresponding google sheets, with
url:
https://docs.google.com/spreadsheets/d/SPREADSHEET_URI/edit#gid=SHEET_URI

We also have blacklist groups for all, custom search, youtube, and
podcasts. By grouping target fields, you can tell the Django application
that they are related together in some way, which will be useful in the
synchronization step (see below). Target fields can be placed in
multiple groups, and groups can contain multiple target fields.

There is also a blacklist_item table: **I'm not sure where or if this is
used.**

[*https://gitlab.com/the-syllabus-scripts/backend/-/wikis/Blacklist-system*](https://gitlab.com/the-syllabus-scripts/backend/-/wikis/Blacklist-system)
**seems to be quite out of date**

