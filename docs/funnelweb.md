# Tech Stack

The backend is a Django app, but is not really used as a webserver, Django is just exploited for its DB ORM. It's really more of a collection of utils, which are called periodically (manually or via recurring cron job) in the form `python manage.py jobname` -- the job will do something like scrape something, upload something to a google sheet, update or fetch from the db, etc.

Fedya is in charge of it and is pretty cautious about big changes, so it's still on python 3.8, while funnelweb is 3.11, and thus uses some more modern pythonisms, is primarily async, more strongly typed, etc. The backend includes many scrapers, but many are site-specific, or use site-specific APIs. This is good for major sites, like google or something, but ideally we don't want to write hundreds of different scrapers for individual websites.

`funnelweb` is a web scraper, and it is totally separate from the backend infrastructure, because the mid-term plan is to rewrite the backend from the ground up. We are starting to do more work with it and need to test and configure it with more sites.
Because it is separate, it lives in its own repository (and all other new tools we create go in their own repos as well). It is, however, in the process of being implemented inside the Django backend so that it can also be called as a weekly cronjob.

It's designed to be a domain-agnostic scraping tool, with as little code as possible written for the purpose of handling specific websites. Instead, for a new domain/subdomain, you write a new section in the YAML config. In a perfect world, you write a basic config for a new domain, run funnelweb on this domain with the debug stuff enabled, identify problems, fix config, and repeat till it's perfect. Then you can disable debug and actually upload the results for this and other domains.

1. `funnelweb` has A LOT of configuration options (some more useful than others) that are well-documented in the readme.
2. Danny also spent some time making it print handy colourful messages to console while it runs, which should make it much easier/faster to debug and work with.
3. So: please do read the `README.md` before much else.

Right now, we're only worrying about `bad_url_mode` and the 'normal' mode. The other modes (`junk_bin_mode` and `url_list`) are not relevant yet (possibly ever). The code itself is just 'alpha', not yet used in production and thus not yet totally stable.

`bad_url_mode` is the most-tested, as normal mode involves making many OpenAI requests which cost us money. To be clear: be mindful, `bad_url_mode` is free but normal mode is not! When using normal mode, if you're testing, you can use `max_per_site: n` setting to ensure it only sends `n` URLs through to the LLM. Only do `max_per_site: -1` in normal mode when running at full scale, or we could use up all our openai credits rather quickly

As mentioned above, we now need to start configuring funnelweb for more domains, mostly journalism-related. To do it, you'll begin in `bad_url_mode`. The idea is to store in the DB a huge list of all possibly publishable URLs that are already online as of a given day. That way, when we run the tool in 'normal mode' next week, we know to ignore the old URLs.

The process of configuring a new domain looks like this:

- You add each to the config.yml file
- Write a config that will correctly fetch all relevant URLs
- Divide into `content`, `no_content` and `no_scrape`.
- The term `no_content` is a bit misleading; `no_content` means, this page itself is not a content page, but we should scrape it because it may contain links to content pages.
- `no_scrape` means ignore, don't bother scraping this URL.
- `content` means it's potentially a good URL.

Again, in `bad_url_mode`, webfunnel saves the URL to db; in normal mode, it gets the page and sends it to an LLM (OpenAI API) to extract its metadata and put into an output pandas dataframe.

For sites with a clear URL hierarchy (more common in 'document' domains, you can set `max_depth: n` to only traverse so far. For a lot of journalism though, the hierarchy isn't so clear, so you might need a larger `max_depth` to make sure we cast a wide net.remember that we're interested in long-form, intellectual content. For things like short blog posts, image galleries, etc., set to `no_content` (if they may have content links on them) or `no_scrape` if they can be ignored completely.

For some domains, we can tell that a URL is old because it has a date/year in the URL -- though we don't want to publish anything from 2023 anymore, I still often allow these previous-year-URLs to be collected as bad URLs, mostly for the sake of completeness, so we have more URLs in the DB in general. But if you want, you can exploit the existence of years/dates in URLs to limit to just 2024+, esp if a site is huge and it's taking many hours to scrape because it has links going back 10+ years. Use your best judgement -- for many `document` domains, there are only a few hundred documents per site (so it seems like we may as well harvest them all); for journalism, there could be 100k+, so filtering on dates found in URL might make more sense, to save time.

`processes: n` sets how many simultaneous processes can run. You can set it in general config and in domain specific config. Set it to 1 if the site has any kind of ratelimiting that prevents us making many requests. In that case also increase `wait` setting. Some sites don't care how quickly and how much we scrape, for these, you can set more processes, less wait time, etc. Ideally we want to have an awesome config.yml file (or more than one, maybe one per language, one for journalism, whatever) that we can run each week to fetch latest URLs and then also add these URLs to the `public.funnelweb` table of old URLs.

## Database

The database lives on AWS and can be explored with `DBeaver` (or a similar DB tool of one's choice). Funnelweb connects to it: this connection is configured using the postgres settings in the config.yml. Funnelweb tables are called `public.funnelweb` (stores old URLS) and `public.funnelweb_run` (which stores metadata about the runs). If you somehow do some kind of 'bad run', you can use the run's `id` to delete all the URLs that it generated…

For each run, the date/time is stored, some sort of UUID is generated, and then the JSON config is put into funnelweb_run and all the bad URLs are put into funnelweb. The date isn't reliable but also not really needed -- we just need to know it existed at the time funnelweb was run. We don't always end up with a timestamp for every url, that's ok. The id is just first 8 chars from a uuid4, used as foreign key to link urls to the run that generated them, so you can find the config used to generate a url if need be. When we do start running in normal mode, it does all this, but also sends the content urls to openai to extract metadata, so we can get it into the review sheet for reviewers, and also apply scoring/classification measures eventually

Regarding use of funnelweb, it's handy to know that config values not set in a specific domain fallback to the general config. So in general you can set disabled: true, then if you're just testing one domain, for that domain only set disabled: false and all other domains will be ignored. You can also split into numerous config files it if helps you. Any way you want to organise for development is fine, but eventually in prod we should have one giant YAML file with all domains properly configured.
