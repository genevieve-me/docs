The Syllabus is very reliant on the Google Custom Search Engine (aka Programmable Search Engine) services to access articles, journalism, and books. Each custom search engine can either search only certain (sub-)domains, or blacklist a list of domains. It is accessed via its ID, referred to as `cse_id`. The service allows users to create and configure as many search engines as possible.

# Quick Start

- Go to the ["working query setups" google sheet](https://docs.google.com/spreadsheets/d/1X7UDXV-7vyL1ESVlFmEXqteJygt79BYlm_HZ8MU_BUY/)
- Enter a table of Google search queries, with columns "query," "unit," "language," "is_academic," "is_niche," "cse_id." For the simplest case, all columns besides query are optional
- Go to <https://programmablesearchengine.google.com/u/1/controlpanel/overview>
- Create a custom search engine. Add the list of domains you want to search
- Copy the ID and paste it into every row under `cse_id` in the google sheet
- Look at the SQL database. Open the `cse_id` and `unit` tables. A "unit" is a logical categorization of queries (eg `it_podcast` for italian podcasts)
- Find the ID of the search engine and units you want to assign to these queries. If creating a new CSE or unit, add them to these tables.
- Start the python backend with `python manage.py runserver`
- Log in at `localhost:8000/admin` with `fedirorlov / 4065251Fo`. Click "add query set-up."
- Input the information for your query set. The "token" is a unique identifier for this set, which by convention is the same as the name of the Google Sheet. The scraper is `CSE (API)`, the frequency is `Batch`, and the mode is content. The `cse_id` and `unit` must also be selected here if they are blank in the google sheet.
- You also need to select how old results can be: this takes the format `15d` (last 15 days) or `2m` (last two months). You cannot write `1m15d`: use `45d` instead.
- We are now ready to process the query set - run `python manage.py upload_queries --set "YOUR_TOKEN"` and the backend will fetch them from google sheets and upload them to our database.
- Now run `python manage.py process_queries --setup "YOUR_TOKEN"` and the backend will begin running these queries and scraping the results into the database.
- Once the command is complete, you can get your results out of the database with `python manage.py create_cse_output --setups "YOUR_TOKEN" --dates "yyyy-mm-dd"` where the date is the date of scraping.
- If scraping took multiple days, or you want more results, try `--dates "2024-01-01,2024-01-03`.

# Google Custom Search Engines Documentation

## Initial Setup

Go to <https://programmablesearchengine.google.com/controlpanel>, create a new engine, then customize it.

We want to look at:

- Sites to include : paste in our list of sites and save
- Look at our SQL database (e.g. in DBeaver)
- go to `tables -> cse_id`, get the search engine ID from google admin panel, and copy paste it in as the `cx` value
- can also put in appropriate `title`, `description`, etc.

Custom search engines cannot be shared within a google organization. Nikolai owns the majority.
Go to `https://programmablesearchengine.google.com/u/1/controlpanel/overview?cx=<some-id>`.
(note that CSE adds wildcard subdomains when no subdomain is specified. This is desired.)

Because the CSE interface is not great, it's preferred to format your list of URLs as an XML file and upload it (under 'annotation').
Note that CSE does not let you add a list if there are duplicates or some URLs are already present.

Now go to "working query setups" in Google Sheets, create a new sheet containing your queries. (Or go to it if it exists).
Each row has a `cse_id` (as above) and a `language`.
`unit` refers to the module (one of our categories).

(There's also `is_acad/niche` which will tag the results in the DB).

This is what the backend will use to scrape, sending queries to a certain CSE (and optionally setting the language).

The backend will be told to use this "query set-up" (a google sheet) by searching the `query_setup` table in the DB, and passing the name on the CLI.

Within each google sheet, you might want to group queries into several different `unit`s so that they can easily be filtered in the output. This is because
the results from the scraping are saved to the DB with what unit they belong to, what query setup they came from, the date of scraping, and their language.

Although the backend will prefer the `unit` and the `cse_id` from the sheet, they must also be put into the DB.

You have a row in `unit` with a token, and list that token in the `query_setup` row. Similarly, there is a row in `cse_id`
with the CSE ID under `cx` and a unique identifier under `id` (just an integer). That can also be in `query_setup` to find the right CSE.

The presence of `unit` and `cse_id` in the DB is a pre-req for uploading a query setup. A string with the search tags and results is accompanied by a certain `unit` and `cse_id`. If they do not exist, we are able to upload that row.
The backend sees what we searched for, which unit the result will belong to, and which cse_id will be used.

In the django panel, you can only specify one CSE ID, but occasionally, we will send queries to different CSE IDs within one query-setup.

#### Aside:

Open the `module` table in our SQL database to see them all.
each module is within a section and corresponds to a category, e.g., "Food and Nutrition" or "The Corporation," etc. Each of them has a code: `SXMY` where `X` and `Y` are integers from .
Look at the ["Uber Section and Modules Master Sheet"](https://docs.google.com/spreadsheets/d/1ho5agI7lixCiX1bOpScKph5lTsJOtRpJe5gtmr66gMY/edit#gid=838263103) to view what they all correspond to.

When testing query set-ups, we can use a `module_id` of `1`. We run other regular queries for each module, and once we have a known-good query set-up for a module, we can change it (according to the id in the `modules` table).

To do that, find your module (eg Smart Cities), find the code (S6.M6), go to modules in the DB, and find the `id` corresponding to `S6.M6`.

Within one query, there may be several different `unit`s in Google Sheets. The unit determines what set of domains it's going to. Put those in the backend as needed. You can also vary the language per query.

Now go to the `unit` table. The `module_id` will always be `1` as mentioned above in this table.

- Add a row and increment the `id` and `number`, put in a `description` and `token`. The token is the entry for the "unit" column in google sheets.

If there are multiple, add them all to the database, repeating this process.

## Adding query set-ups via backend

Run our python backend and run the `runserver` command. That starts a webserver on port 8000 on localhost.

Go to `localhost:8000/admin` in your browser, log in with `fedirorlov / 4065251Fo`

Go down to queries/query set-ups, go to that link, then click "add query set-up" in top right.

The table you see here is the `query_setups` table in the database. Adding a query here will add it to the database.

Also add the:
- description (for your own reference)
- token (a unique string identifier for this query-setup
- scraper: select `CSE (API)`
- frequency: `Batch` from the drop-down menu. We use batch for everything since we don't currently have an automated system for recurring scraping.
- scraping goal: this determines the output. If you set it to publisher discovery, the output will not list all the discovered URLs, but a table of domains showing what type of results we got for each. Content discover gives a table of URLs.
- Update mode should always be set to renew.
- Language/unit/cse can be specified in spreadsheet rows, leave them blank here. (They can be overridden here if you need: the drop-down shows those that are available in the corresponding DB tables)
- Spreadsheet URI and sheet URI: unique tokens in the GSheets URL, e.g. https://docs.google.com/spreadsheets/d/**1kk0hwRwgESak5DlACrzzQ4ro8H6RkAizPm974rmFLjg**/edit?pli=1#gid=**2088335442**. So that the backend can find our queries
- Search start/end date only work for youtube. We set these to today.
- Cse period is a string of the form `d15`, `m2`, etc. You specify a period days/months/years, and an integer. The results will be filtered so they must be at least that recent. You can only pass one: `d15,m2` is invalid, instead type `d75`. This is required.
- This gets passed to the CSE (parsed into google format: like `since:YYYY-...`). (This is different from the arguments we pass to `create_cse_output`, which can filter the results based on what date they were scraped, not the date the content was posted on the internet.)

You can compare this to another query setup from this management interface if unsure about anything.

Then run `manage.py upload_queries --setup="token"` for whatever token you just put into Django (and therefore into the `query_setup` table).

The `process_queries` does the actual scraping (this command is pure python, it works with the CSE API). The syllabus has a linux server for running them, so you're not leaving your computer on overnight.
The results from this step are saved into the database.

Then `create_cse_output --setups "token" --dates ...`. This fetches results from the database and saves them into a local .xlsx file.
Documentation is available with `create_cse_output --help`.

# New explanation

Query setups are tables with a list of queries, associated languages, a "unit", and the CSE ID (which google custom search engine to run the query on). The unit is a unique identifier telling the backend which queries to run. For convenience, we are storing these tables in Google Sheets.

The database has a table called `unit`, with columns `id`, `number`, `description`, `token`, and `module_id`.
All besides `token` seem to be legacy/unused - all that matters is the `token`, which corresponds to the "unit"
in our google sheet.

Theoretically, the CSE ID should also be in a new row in the `cse_id` table under the `cx` column. But there are plenty of CSE IDs in google sheets not in this table, and there are `id`s in `unit` which do not correspond to `id`s in `cse_id`.

### `create_cse_output`

The command fetches data from the database, processes it in a certain way, calculate scores and outputs 3 files:
cse_results_<date>.xlsx - unique sources with scores and other basic data.
extended_cse_results_<date>.xlsx - unique sources with extended data such as top modules. The most useful output file. Unified-CSV like format.
analytics_cse_results_<date>.xlsx - output with domain analysis: sources per domain, top modules, source examples. 

