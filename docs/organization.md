# Products

## Weekly curated links

![Weekly harvesting by content type and language](assets/curation.jpg)

Each week, we select around 700 new releases of six types: mostly
longform writing, followed by videos, and a smaller amount of books,
documents (e.g., academic articles, NGO reports, etc.) and podcasts.
These are sorted into around 60 "modules," which are categories like
globalization, automation and labour, etc. Those modules are grouped
into six sections (e.g., Tech or Arts).

The feed curation for each end-user is currently quite simple: they
select the languages, types and modules they want to receive. For
example, one user might want only French podcasts and videos about
urbanization, film, and cybersecurity.

This one week production cycle is currently the core of the Syllabus.

## Partners

Our biggest partner is the Open Society Foundation.

### Archives

We create archives of the content we unearth and quality check for some
institutional partners. For more information, see [the archive docs](archives.md).

-   Friedrich Ebert Foundation (FES) in Germany

-   Center for Digital Rights in Canada

-   Open Society Foundation (personally tailored by Evgeny and Ekaitz, found with CSEs, needs to be automated)

### Niche curation

We also do curation for some other partners on a small scale and on a niche topic.

- We provide a list of links to Renault for their newsletter: the topics vary, the content type is typically podcasts and industry reports.

- We also provide content for a "LatAm Daily Briefing" newsletter run by Jordana Timerman.

Some of this is currently done 100% by hand!

# Staff

## **Directors**

Managing

-   Evgeny Morozov (head of curation)

-   Marco Ferri (management, funding, tech, jack of all trades)

-   Ekaitz Cancela (curator, particularly for books and Spanish-language content)

Advisory

-   Renata Ávila

-   Hans de Zwart

## **Tech Team**

Full-time and/or major contributors

-   Fedir Orzov: lead dev since 2020, wrote backend, oversees all python code

-   Danny McDonald: major contributor since mid-2023, has been working
    > on new NLP/ML-related projects for curation (topic analysis,
    > content scoring), as well as tools for web crawling

-   New dev: will work on overhauling backend with Fedir

-   Bohdan: now part-time, but did a lot of initial work on the Django backend and scraping

-   Yunus: web backend (Java), e.g. payments

-   Matteo: web frontend

Part-time

-   Carsten (NLP)

-   Oliver (Google scholar alerts)

## **Curation Team**

For reference, reviewing and selecting all the content in one language
for a week takes about 10 hours total, so curation is largely part-time
work.

-   Evgeny: English academic articles, final proofing of all content selections

-   Ekaitz: most of the rest of English content and all Spanish content, client projects, special editions

-   Begum: English journalism

-   Alessandra & Moren: French

-   Gustavo: Portuguese

-   Pierluigi: Italian

-   Lela & Lukas: German

-   Max (podcasts)

## **Other**

-   A full-time new hire (special projects manager)

-   Ivan Krasnov: communication (e-mails, socials)

-   Jamillah & Edwin: part-time data cleaning

# **Communication**

-   Not 100% tied to any ecosystem right now, but the below has been
    > working well

-   Slack free tier, with one bot (alerts about errors in sheets): might
    > go to pro and also set up more bots

-   Google workspace (Drive, Calendar): especially important for the
    > google sheets infrastructure

-   Slack is also used for ad-hoc file exchange, a lot of our work is in
    > Slack direct messages

Each team (tech/curation) has a monthly meeting, and there is a general
staff meeting every two months.

# Data sources

See [data sources and pipelines documentation](data-flow.md).

# **Tech Stack**

The core function of the tech stack is to produce spreadsheets of
content that can be manually reviewed by curators, to assemble those
\~700 pieces for our weekly edition. Those spreadsheets have additional
information, from the basics like the language, type of content, source
to more complex endeavours like a "quality score."

## **Data Pipeline Overview**

Depending on where the data "lifecycle" starts, we have several
pipelines.

**TODO - this is the key and what I really don't understand 100% yet**

Google Scholar Alerts

-   Email is sent about a new article

-   →  Basic article metadata added to google sheet (**automatically done? how?**)

-   →  Backend, (uses that metadata to find and scrape the content)

-   →  Backend (analyzes the content and adds some metadata, e.g. topic, score)

-   →  Auxiliary Google sheets (currently manually copy pasted by Nikolai)

-   →  Unified sheets

Google Custom Search Engines

All organized around "query setups." Query sets can be created in google sheets, but they are uploaded to the database by the backend, and scraping is also done by the backend (which uploads results to the DB as well). See [custom search engine docs](google-cse.md).

Backend scraping Amazon

**TODO**. Also revoles around query setups, but they are just sent to Amazon instead of the Google API.

## Google Sheets

The syllabus relies on tabular data for almost everything. Google Sheets
is the main place for reviewing content, displaying scraping results,
storing blacklists etc.

In our tables, we use a special universal table structure, which is
called Unified CSV format. It has columns that can only be applied for
specific content types. **TODO: what does this mean?**

Auxiliary sheets

-   Formulas for blacklisting (match certain strings)

Review sheets

-   Todo

## **Backend**

The backend is a Django app, but is not really used as a webserver,
Django is just exploited for its DB ORM. It's really more of a
collection of utils, which are called periodically (manually or via
recurring cron job) in the form python manage.py jobname -- the job will
do something like *scrape* something, *upload* something to a google
sheet, update or *fetch from the db*, etc. Specifically, the backend is
typically run on an end user's machine.

Fedya is in charge of it and is pretty cautious about big changes, so
it's still on python 3.8, while Danny's projects tend to be on 3.11, and
thus use some more modern pythonisms, are primarily async, more strongly
typed, etc. The backend includes many scrapers, but many are
site-specific, or use site-specific APIs. This is good for major sites,
like google or something, but ideally we don't want to write hundreds of
different scrapers for individual websites.

### Wikis

Most of the wikis are out of date, but the query system is documented:
[*https://gitlab.com/the-syllabus-scripts/backend/-/wikis/Query-system*](https://gitlab.com/the-syllabus-scripts/backend/-/wikis/Query-system)

**TODO!**

### Misc

#### Slack Bots for TS Backend Alerts

- **TODO**: Backend alerts and Slack bot

Data not found on GS tables
- Certain cronjobs send an alert if there is an error
- For example, Inoreader constantly fails to generate data, and the podcast curator is pinged
- Occurs for any scraper run via a cronjob, eg `scrape_ssrn`
- **TODO:**  make a list of all the scrapers currently being run on recurrent jobs

Reviewer Sheets filtering report
- Tells Marco how many rows trvel from the review sheets to the unified sheets, split by language and content type
- To make sure we have content for the week

Sunday Unified Sheet Checks

1. Which categories/languages in unified sheets have too few results
2. Amount of missing metadata
3. If any language/content pair is totally empty

## **Funnelweb**

**TODO:** unify with [funnelweb docs](docs/funnelweb.md).

funnelweb is a web *crawler* & *scraper*, and it is totally separate
from the backend infrastructure, because the mid-term plan is to rewrite
the backend from the ground up. We are starting to do more work with it
and need to test and configure it with more sites. Because it is
separate, it lives in its own repository (and all other new tools we
create go in their own repos as well). It is, however, in the process of
being implemented inside the Django backend so that it can also be
called as a weekly cronjob. (**TODO: **what does the backend calling
funnelweb look like? On what server is the cronjob run?)

It's designed to be a domain-agnostic scraping tool, with as little code
as possible written for the purpose of handling specific websites.
Instead, for a new domain/subdomain, you write a new section in the YAML
config. In a perfect world, you write a basic config for a new domain,
run funnelweb on this domain with the debug stuff enabled, identify
problems, fix config, and repeat till it's perfect. Then you can disable
debug and actually upload the results for this and other domains.

1.  funnelweb has A LOT of configuration options (some more useful than
    > others) that are well-documented in the readme.

2.  Danny also spent some time making it print handy colourful messages
    > to console while it runs, which should make it much easier/faster
    > to debug and work with.

3.  So: please do read the README.md before much else.

Right now, we're only worrying about bad_url_mode and the 'normal' mode.
The other modes (junk_bin_mode and url_list) are not relevant yet
(possibly ever). The code itself is just 'alpha', not yet used in
production and thus not yet totally stable.

bad_url_mode is the most-tested, as normal mode involves making many
OpenAI requests which cost us money. To be clear: be mindful,
bad_url_mode is free but normal mode is not! When using normal mode, if
you're testing, you can use max_per_site: n setting to ensure it only
sends n URLs through to the LLM. Only do max_per_site: -1 in normal mode
when running at full scale, or we could use up all our openai credits
rather quickly

As mentioned above, we now need to start configuring funnelweb for more
domains, mostly journalism-related. To do it, you'll begin in
bad_url_mode. The idea is to store in the DB a huge list of all possibly
publishable URLs that are already online as of a given day. That way,
when we run the tool in 'normal mode' next week, we know to ignore the
old URLs.

The process of configuring a new domain looks like this:

-   You add each to the config.yml file

-   Write a config that will correctly fetch all relevant URLs

-   Divide into content, no_content and no_scrape.

-   The term no_content is a bit misleading; no_content means, this page
    > itself is not a content page, but we should scrape it because it
    > may contain links to content pages.

-   no_scrape means ignore, don't bother scraping this URL.

-   content means it's potentially a good URL.

Again, in bad_url_mode, webfunnel saves the URL to db; in normal mode,
it gets the page and sends it to an LLM (OpenAI API) to extract its
metadata and put into an output pandas dataframe.

For sites with a clear URL hierarchy (more common in 'document' domains,
you can set max_depth: n to only traverse so far. For a lot of
journalism though, the hierarchy isn't so clear, so you might need a
larger max_depth to make sure we cast a wide net.remember that we're
interested in long-form, intellectual content. For things like short
blog posts, image galleries, etc., set to no_content (if they may have
content links on them) or no_scrape if they can be ignored completely.

For some domains, we can tell that a URL is old because it has a
date/year in the URL -- though we don't want to publish anything from
2023 anymore, I still often allow these previous-year-URLs to be
collected as bad URLs, mostly for the sake of completeness, so we have
more URLs in the DB in general. But if you want, you can exploit the
existence of years/dates in URLs to limit to just 2024+, esp if a site
is huge and it's taking many hours to scrape because it has links going
back 10+ years. Use your best judgement -- for many document domains,
there are only a few hundred documents per site (so it seems like we may
as well harvest them all); for journalism, there could be 100k+, so
filtering on dates found in URL might make more sense, to save time.

processes: n sets how many simultaneous processes can run. You can set
it in general config and in domain specific config. Set it to 1 if the
site has any kind of ratelimiting that prevents us making many requests.
In that case also increase wait setting. Some sites don't care how
quickly and how much we scrape, for these, you can set more processes,
less wait time, etc. Ideally we want to have an awesome config.yml file
(or more than one, maybe one per language, one for journalism, whatever)
that we can run each week to fetch latest URLs and then also add these
URLs to the public.funnelweb table of old URLs.

## Database

The database is PostgreSQL and hosted on AWS. It serves as the historical reference for our modules/categories, our queries, etc. This is because it also hosts all scraped data, and associates those pages with the queries that produced them and as much other metadata as possible.

For details, see the [documentation on the database, which also covers query set-ups and web scraping.](db-and-queries.md)

# Workflow in Detail

## Academic Articles

- Filter english review backup sheet by type

[See the docs on academic articles.](acad-articles.md)

# Google Drive layout

-   **TODO:** The per-language review sheets. <https://docs.google.com/spreadsheets/d/1jY6KBdvLYagILmszuFFNVHpXi-NScbCakayxmJNXIUg/edit> and [Russian_review](https://docs.google.com/spreadsheets/d/1zBO3tu8qGhZK7grouVzStfZ7FTYzzzIXRSE5J66jwXA/) etc...

Quality control column in review sheets: if marked yes, content goes to Unified Sheet, if no, it goes to a [Unwanted_items](https://docs.google.com/spreadsheets/d/1wovFZE-I4HLytnSlOW1a8AWfVl7ucA0DJ50k0_UwTC8/edit#gid=0) google sheet, if blank, it just gets stored in the database.

-   The list of modules is in [New Unified Sheet - January 2023](https://docs.google.com/spreadsheets/d/1H7a77skKf0XVDts8ZlBAZskBJ7zbLAbc3FNML8a9bt0/edit#gid=30001760), in the modules tab/subsheet.

- As well as content for each week

-   While query setups are archived to the database, and can be uploaded from any google sheet (or theoretically any tabular data, or even by directly interfacing with the DB and bypassing our backend software), by convention they start life in [working query setups](https://docs.google.com/spreadsheets/d/1X7UDXV-7vyL1ESVlFmEXqteJygt79BYlm_HZ8MU_BUY/) and remain there for reference or easy editing.

-   There is also a [Sections and Modules Master Sheet](https://docs.google.com/spreadsheets/d/1ho5agI7lixCiX1bOpScKph5lTsJOtRpJe5gtmr66gMY/edit) which seems to duplicate the subsheet in the "Unified Sheet"

-   [The center for digital rights archive](https://docs.google.com/spreadsheets/d/1wXOUvZ-wfgcRMlwNJsg6CAKtRALn_Gkdik5tSo0TU4E/edit#gid=153406143) (see [archive docs](archives.md)).

-   **TODO:** [Youtube URL Blacklists](https://docs.google.com/spreadsheets/d/1T-dd5KCMiZOQbyJSGrUIu17HD9gBtl_4X_n3G7SuIwA/edit)

-   **TODO:** [manual_items](https://docs.google.com/spreadsheets/d/11SCCkTzKcFSMN97QPl5RbKOmc5grU-_347rO2ujdzYI/)

-   **TODO:** [manual_items](https://docs.google.com/spreadsheets/d/1wovFZE-I4HLytnSlOW1a8AWfVl7ucA0DJ50k0_UwTC8/)

-   [Cronjobs Schedule](https://docs.google.com/spreadsheets/d/1kEihCxKzEq3JzeVXgLRPo9Ogo7p4eQnuMfkeA_A_Hro/). **TODO:** create `server_infrastructure.md`.

Related Google services:

Diagnostics - statistics about what we harvest each week, by language/content type. For trends, this is how we do graphs/etc.


- Shared Google Calendar

- [Programmable Search Engine Control Panel](https://programmablesearchengine.google.com/u/1/controlpanel/overview) (this cannot be shared organizationally! one more reason to migrate from it)

# Challenges

The common thread with all of the above is: too much manual busywork!

## Clunky curator workflow

This is related to our google reliance.

-   Anytime they want to blacklist something, we have to go through a
    > labor-intensive process which might involve manually committing to
    > the backend

-   This is not scaleable or sustainable

-   The same applies to whitelisting

-   Lots of little frictions add up

## Backend

-   This is both a challenge and an opportunity

-   We want to rewrite it, and remove the dependency on Django, which was never really meant for this use-case

-   Hiring a new senior dev to take ownership of this

-   We don't have testing infrastructure for query performance

-   We don't match queries to results in a 1-1 manner, even though we could do this. We try to figure it out after the fact with a custom script

## Scraping failures

**Academic publishing ecosystem**

-   Publishers work very hard to guard their metadata, and accessing
    > content is even worse

**Overreliance on Google**

-   As mentioned above, hacky solutions and our accounts are sometimes closed

-   E.g., using a hack to override google rankings and just get all the raw results, then filter them ourselves

-   This often involves manual work

-   Eventually, we hope to totally get rid of scholar alerts too

### Bad queries

Lots of queries that either provide zero results (as much as 30%) or
queries that are so broad they provide too many junk results we must
manually filter (another 15%). Providing zero results also has a cost:
just CPU time in the best case, tiresome manual oversight (resetting the
backend if it errors out, manually solving CAPTCHAs) in the worst case.

Queries can be good for initial discovery, but they're too noisy and a black box.

We are scraping 100,000 pieces of content and only keeping 550, so we still have to add manual content!
(Almost 20%, far too high). This problem is worst when it comes to books and podcasts.

# Opportunities

## New Institutional Partners

-   Ford Foundation

-   Hewlett Foundation

-   EU grants for libre/FOSS software

-   Taylor (?) University

-   Another foundation? In my notes somewhere **TODO**

#### Goals (from an old gitlab wiki)

1.  Advanced documentation on each of the tools we use.

2.  Organizing, generalizing, ordering, uniting tools and projects.
    > Making them use as much common stuff as possible, delete code
    > duplications.

3.  Database related code optimisations and stability fixes.

4.  Minor reorganizing changes in the database.

5.  Automation of weekly scraping cycle processes with a system of
    > cron-jobs.

6.  Development of new tools and features (scrapers, processors, NLP
    > classifiers).

7.  A common place to store SQL commands.

# Misc

-   Holidays are two weeks each in August/December, as well as local
    > holidays

