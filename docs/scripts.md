# Non-Django tools and projects

### Enrichment


This tool is used to fix Unified CSV files’ data. For example, it fixes capitalization in titles, it fixes names of authors, it can detect an item’s language using various translators, delete HTML code in the text, etc. In other words, this tool fixes and enriches data from an input.csv file. There are many different rules on how to treat bugs in data, so it’s better to check the source code. The project’s repo: https://gitlab.com/the-syllabus-scripts/enrichment

### Articles Filtering

<https://gitlab.com/the-syllabus-scripts/articles-filtering>

This tool reads two input CSV files, where one is a “filter” and the other one is an “initial” file to filter. The tool deletes rows with known values for some of the columns in the initial file. Known values - those that are present in the filter file. 
The tool also creates a new column in the initial file - publisher_known. It fills it with “yes” if a publisher is present in the filter file publishers, “no” otherwise.

The tool accepts 2 input CSV files and one text file. The files are:
initial.xlsx/.csv - this input file contains a Unified CSV like table. This file is to be modified, filtered and outputted.
filter.xlsx/.csv - it contains known data and is used as a filter. Doesn’t get changed or outputted.
columns_to_analyze.txt - a text file with column names to analyze and filter.
The tool outputs 1 file - initial_filtered_<date>_<time>.csv.

### Module Predictor



[The module predictor tool](https://gitlab.com/the-syllabus-scripts/module-predictor) predicts modules of items from input.xlsx/.csv file. It uses an NLP model to do it: see [the module-classifier-api repository](https://gitlab.com/the-syllabus-scripts/module-classifier-api). You should choose what fields to use for classification in the special input file - columns_list.txt.

The tool concatenates all the columns, mentioned in the columns_list.txt file, into one string and uses a trained model to classify its module. The tool writes the top 3 modules with their probabilities at the end of the file, appending 6 columns to the table. 
To run the tool, you need to install the package with the classifying model in your local virtual environment. There is still no centralized storage of the package with the model, so you can download the last actual version from here. Just put this .whl file in the root folder of the project, go to your virtual environment in the terminal and do pip install module_classifier_api-0.0.3-py3-none-any.whl.

### Other Django tools

***TODO***: some of these are likely outdated, I'm not sure which.

There are many other Django tools we have, but they are not that important and frequently used. Some of them work as a part of the tools described above.

- Google Books scraper. We use Google Books service to find new books. We use both API way and Selenium based methods. This tool has extended settings in the gbooks_settings database table. To find out more about this tool, please check this Wiki page.
- Inoreader scraper. Inoreader is a special service for gathering and monitoring podcasts. We use this service to obtain fresh episodes from the podcasts we are subscribed to. We have commands for checking channels and uploading fresh episodes to Google Sheets. There also is a command which can subscribe an account to a list of new podcasts. To read more, check this Wiki page.
- Newsletters generator. This tool reads input data from Google Sheets and creates an HTML file that we use to send out to our clients (both paid and free users). It uses Django renderer to render HTML. This tool is used once a week.
- Wikipedia scraper. This tool reads a list of Wikipedia pages URL and looks for different links in the text, especially in the “See also” section. In other words, it’s used to find related terms that then can be used for creating new search queries. To read more, check this page.
- PDF metadata fetcher. We use this tool quite frequently to find out how many pages are in PDF files. This tool receives URLs, analyzes them and saves output data to an CSV file. To read more about this tool, check this Wiki page.
- YouTube. This application has several useful scripts and commands for scraping YouTube. There are commands for scraping search queries with YT API, scraping separate videos data with/without the API, checking channels’ RSS feeds for fetching fresh videos for needed channels. To read more, check this page.
- Django Admin panel. We have a very simple GUI that may help non-technical users to interact with the database (mostly with queries, modules and settings). You need to have an admin account to access it. We don’t have it running centralized yet, so you will need to run it on your computer with python manage.py runserver and then go to http://127.0.0.1:8000/.


